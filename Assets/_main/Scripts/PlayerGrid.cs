using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrid : MonoBehaviour {

    [SerializeField] private GameObject gridObject;
    [SerializeField] private GameObject horizontalLine;
    [SerializeField] private GameObject verticalLine;
    [SerializeField] private Vector3 gridObjectInitialPosition = new Vector3(-7, 2.5f, 0); // -7, 2.5, 0
    [SerializeField] private Vector3 horizontalLineInitialPosition = new Vector3(0, 3.5f, 0); // 0, 3.5, 0
    [SerializeField] private Vector3 verticalLineInitialPosition = new Vector3(-8, -0.5f, 0); // -8, -0.5, 0

    private Player _owner;

    public Player Owner { get { return _owner; } set { _owner = value; } }

    [ContextMenu("Generate Grid")]
    private void GenerateGrid() {
        int offsetX = 0;
        int offsetY = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                Instantiate(gridObject,
                    new Vector3(gridObjectInitialPosition.x + offsetX, gridObjectInitialPosition.y - offsetY, gridObjectInitialPosition.z),
                    Quaternion.identity,
                    gameObject.transform);
                offsetX += 2;
            }
            offsetX = 0;
            offsetY += 2;
        }

        offsetY = 0;
        for (int i = 0; i < 5; i++) {
            Instantiate(horizontalLine,
                new Vector3(horizontalLineInitialPosition.x, horizontalLineInitialPosition.y - offsetY, horizontalLineInitialPosition.z),
                Quaternion.identity,
                gameObject.transform);
            offsetY += 2;
        }

        offsetX = 0;
        for (int i = 0; i < 9; i++) {
            Instantiate(verticalLine,
                new Vector3(verticalLineInitialPosition.x + offsetX, verticalLineInitialPosition.y, verticalLineInitialPosition.z),
                Quaternion.identity,
                gameObject.transform);
            offsetX += 2;
        }
    }
}